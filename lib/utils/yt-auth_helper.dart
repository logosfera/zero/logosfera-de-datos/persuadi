import 'package:yaml_writer/yaml_writer.dart';

import 'dart:io';

class YtAuthHelper {
  String? clientID;
  String? clientSecret;

  YtAuthHelper(this.clientID, this.clientSecret) {}

  void buildAuthFile() async {
    var yamlWriter = YAMLWriter();

    var yamlDoc = yamlWriter.write(
        {'clientId': clientID, 'clientSecret': clientSecret, 'code': ''});

    final filename = 'youtube-auth.yaml';
    var file = await File(filename).writeAsString(yamlDoc);
  }
}

import 'package:dart_console/dart_console.dart';
import 'dart:io';

class TUI {
  Console console = new Console();
  final ansiEraseInDisplayAll = '\x1b[2J';
  final ansiResetCursorPosition = '\x1b[H';

  TUI() {
    console = new Console();
  }

  void fullscreen(int lineasUsadas) {
    int cont = 1;
    while (cont < console.windowHeight - lineasUsadas) {
      console.writeLine("", TextAlignment.center);
      cont++;
    }
  }

  void demo() {
    console.clearScreen();
    console.setBackgroundColor(ConsoleColor.blue);
    console.setForegroundColor(ConsoleColor.white);

    // Título
    console.writeLine('PERSUADI', TextAlignment.center);
    console.writeLine(' ', TextAlignment.center);

    //Subtítulo
    console.setTextStyle(italic: true);
    console.writeLine(
        "Programa para Extracción de Estadísticas", TextAlignment.center);
    console.writeLine("de redes sociales", TextAlignment.center);
    console.writeLine("sobre Unidades Audiovisuales", TextAlignment.center);
    console.writeLine("Distribuidas en Internet", TextAlignment.center);

    // Línea separadora
    console.setTextStyle(strikethru: true);
    console.writeLine(' ', TextAlignment.center);
    console.setTextStyle(strikethru: false);

    // Firma
    console.setTextStyle(blink: true);
    console.setForegroundColor(ConsoleColor.brightMagenta);
    console.setBackgroundColor(ConsoleColor.brightRed);

    // TCP love you
    console.writeLine("tcpta 07.22", TextAlignment.left);
    console.setBackgroundColor(ConsoleColor.black);
    console.cursorUp();

    // Reubicación
    int cont = 0;
    while (cont < 11) {
      console.cursorRight();
      cont++;
    }

    cont = 0;
    while (cont < console.windowWidth - 23 - 11) {
      console.write(" ");
      cont++;
    }

    // Made by Logos
    console.setTextStyle(blink: false);
    console.setTextStyle(bold: true);
    console.setForegroundColor(ConsoleColor.green);
    console.setBackgroundColor(ConsoleColor.brightRed);
    console.write("Made with love by Logos\n");

    // Reubicación
    cont = 0;
    while (cont < console.windowWidth - 24) {
      console.cursorRight();
      cont++;
    }
    // Web
    console.write("https://www.logos.net.ar\n");

    // Reseteamos
    console.resetColorAttributes();
  }
}

import 'package:persuadi/persuadi.dart' as persuadi;
import 'package:persuadi/tui/tui.dart';

import 'dart:convert';
import 'dart:io';

import 'package:persuadi/utils/yt-auth_helper.dart';
import 'package:yt/yt.dart';
import 'package:logger/logger.dart';

Future<void> main() async {
  // Cargar la configuración
  final configFile = File('config.json');
  final jsonString = await configFile.readAsString();
  final dynamic config = jsonDecode(jsonString);

  // Cargar Interfaz Custom
  final tui = TUI();
  tui.demo();

  // Cargar Interfaz para Logger
  Logger logger = Logger(
    filter:
        ProductionFilter(), // Use the default LogFilter (-> only log in debug mode)
    printer: PrettyPrinter(
        methodCount: 0, // number of method calls to be displayed
        errorMethodCount: 3, // number of method calls if stacktrace is provided
        lineLength: tui.console.windowWidth, // width of the output
        colors: true, // Colorful log messages
        printEmojis: true, // Print an emoji for each log message
        printTime: true // Should each log print contain a timestamp
        ), // Use the PrettyPrinter to format and print log
    output: null,
  );

  // Creando OAuth Config File para Paquete Yt
  logger.v("Cargando Módulo para Youtube");

  YtAuthHelper ytAuthHelper = YtAuthHelper(
      config["youtube"]["auth"]["clientID"],
      config["youtube"]["auth"]["clientSecret"]);

  ytAuthHelper.buildAuthFile();
}
